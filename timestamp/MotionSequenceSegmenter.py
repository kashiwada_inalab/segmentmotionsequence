import csv
import os

new_dir_path_motionprimitives = r"C:\Users\kashiwada\OneDrive\ドキュメント\timestamp\MotionPrimitives"
new_dir_path_motionrests = r"C:\Users\kashiwada\OneDrive\ドキュメント\timestamp\MotionRests"
memoPath = r"C:\Users\kashiwada\OneDrive\ドキュメント\timestamp\takahasi2.txt"
motionSequencePath = r"C:\Users\kashiwada\OneDrive\デスクトップ\nii-dataset_kashiwada\dataset_id_31\MotionSequence_id_31.csv"


os.makedirs(new_dir_path_motionprimitives, exist_ok=True)
os.makedirs(new_dir_path_motionrests, exist_ok=True)

with open(memoPath) as f:
    memo_strip = [s.strip() for s in f.readlines()]
f.close()

with open(motionSequencePath) as f:
    motionPrimitive_strip = [s.strip() for s in f.readlines()]
f.close()

for num_of_motion in range(15):
  path_w=new_dir_path_motionprimitives+"\\"+str(num_of_motion).zfill(2)+".csv"
  #path_w = r"C:\Users\kashiwada\OneDrive\ドキュメント\timestamp\\"+str(num_of_motion).zfill(2)+".csv"
  with open(path_w, mode='w') as f:
    for num in range(int(memo_strip[num_of_motion*2])-1,int(memo_strip[num_of_motion*2+1])):
      f.writelines(motionPrimitive_strip[num]+"\n")

for num_of_motion in range(16):
  path_w=new_dir_path_motionrests+"\\rest"+str(num_of_motion).zfill(2)+".csv"
  #path_w = r"C:\Users\kashiwada\OneDrive\ドキュメント\timestamp\\"+"rest"+str(num_of_motion).zfill(2)+".csv"
  with open(path_w, mode='w') as f:
    if(num_of_motion==0):
      for num in range(0,int(memo_strip[num_of_motion])-1):
        f.writelines(motionPrimitive_strip[num]+"\n")
      print(str(num_of_motion)+"a")
    elif(num_of_motion==15):
      print(str(num_of_motion)+"b")
      for num in range(int(memo_strip[num_of_motion*2-1]),1245):
        f.writelines(motionPrimitive_strip[num]+"\n")
    else:
      for num in range(int(memo_strip[num_of_motion*2-1]),int(memo_strip[num_of_motion*2])-1):
        f.writelines(motionPrimitive_strip[num]+"\n")
      print(str(num_of_motion)+"c")
